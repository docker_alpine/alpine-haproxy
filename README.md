# alpine-haproxy
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721/alpine-haproxy)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721/alpine-haproxy)

### x64
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/alpine-haproxy/x64)
### aarch64
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/alpine-haproxy/aarch64)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64,aarch64
* Appplication : [HAProxy](http://www.haproxy.org/)
    - HAProxy is free, open source software that provides a high availability load balancer and proxy server for TCP and HTTP-based applications that spreads requests across multiple servers.



----------------------------------------
#### Run

```sh
docker run -d \
           forumi0721/alpine-haproxy:[ARCH_TAG]
```



----------------------------------------
#### Usage

* If you want to use HAProxy, you need to create `haproxy.cfg` and add `-v haproxy.cfg:/conf.d/haproxy/haproxy.cfg` to docker option.



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |

